﻿// Learn more about F# at http://fsharp.net. See the 'F# Tutorial' project
// for more guidance on F# programming.
#r @"..\packages\FsCheck.1.0.4\lib\net45\FsCheck.dll"
//#load "GildedRose.fs"
//open GildedRose
open FsCheck


// Define your library scripting code here

type Foo = { Int:int; String:string }

let fooGenerator = 
    Gen.map2 (fun a b -> { Foo.Int = a; String = b } ) Arb.generate<int> Arb.generate<string>
    |> Arb.fromGen

fooGenerator


type StoreItem = {
    Name : string
    SellIn : int
    Quality : int 
}


[<Literal>] 
let Sulfuras = "Sulfuras"
[<Literal>] 
let AgedBrie = "Aged Brie"
[<Literal>]
let BackstagePasses = "Backstage passes"

let nameGenerator = Gen.oneof [ 
    gen { return "An item" }
    gen { return Sulfuras } 
    gen { return AgedBrie } 
    gen { return BackstagePasses } 
]

type MyGenerators =
    static member StoreItem() = 
        { new Arbitrary<StoreItem>() with 
            override x.Generator = 
                Gen.map3 (fun a b c -> { Name = a; SellIn = b; Quality = c}) nameGenerator Arb.generate<int> Arb.generate<int>
            override x.Shrinker t = Seq.empty
        }

Arb.register<MyGenerators>() |> ignore

let ``quality for Sulfuras it always 80`` item = 
    (item.Name = Sulfuras) ==> ((item).Quality = 80)

let ``quality is always positive for valid items`` item = 
    printf "%s" item.Name
    (item.Quality >= 0) ==> ((item).Quality >= 0)


Check.Quick ``quality is always positive for valid items``
